#!/bin/bash

export $(grep -v '^#' .env | xargs -0)

DB_FOLDER="./db"
FILES_LIST=($(ls "$DB_FOLDER"))

echo "Respaldos disponibles en la carpeta /db:"
for i in "${!FILES_LIST[@]}"; do
  echo "[$i] ${FILES_LIST[$i]}"
done

echo "Por favor, seleccione un número correspondiente a un archivo:"
read SELECTED_FILE

DB_FILE="${FILES_LIST[$SELECTED_FILE]}"

if [ -f "$DB_FOLDER/$DB_FILE" ]; then

    echo "Restoring ${DB_DATABASE} -> ${DB_HOST} database in local from ${DB_FOLDER}/${DB_FILE}"

    docker-compose cp ${DB_FOLDER}/${DB_FILE} ${DB_HOST}:/tmp/restore-db.sql
    docker-compose exec -it ${DB_HOST} sh -c "mysql -u root -p\"${DB_PASSWORD}\" -e 'DROP DATABASE IF EXISTS ${DB_DATABASE}; CREATE DATABASE ${DB_DATABASE};'"
    docker-compose exec ${DB_HOST} sh -c "mysql -u root -p\"${DB_PASSWORD}\" ${DB_DATABASE} < /tmp/restore-db.sql;"

    echo Restarting docker...

    docker-compose down
    docker-compose up -d

else
  echo "El archivo seleccionado no existe."
fi
