# PHP Workgroup
It only contains docker php container


## Instalación

* Copy the env file **php -r \"file_exists('.env') || copy('.env.dist', '.env');** and customize if you need it 
* Turn on the containers **docker-compose up -d**

## DCLI Command

Before all, make a new alias in your profile for dcli 
(on Mac nano ~/.bash_profile or nano ~/.zshrc, on Linux nano ~/.bashrc) and add on your token file as shown below

```bash
    alias dcli='docker-compose -f docker-compose.cli.yml run --rm'
```

## Autor ✒️
* **LPinto** *
